FROM ubuntu:18.04 as base
RUN apt-get -y update && apt-get install -y git

RUN mkdir /mfi_data_test

COPY ./requirements.txt /mfi_data_test/requirements.txt

WORKDIR /mfi_data_test
RUN apt-get install -y python3.6
RUN apt-get install -y python3-pip
RUN ln -sfn /usr/bin/python3.6 /usr/bin/python
RUN pip3 install -r requirements.txt --no-cache-dir

COPY . /mfi_data_test

RUN apt-get update && \
    apt-get install -y openjdk-8-jdk && \
    apt-get install -y ant && \
    apt-get clean;

RUN apt-get update && \
    apt-get install ca-certificates-java && \
    apt-get clean && \
    update-ca-certificates -f;

ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/
RUN export JAVA_HOME

RUN apt-get remove -y git && apt-get clean