import json
import datetime
from datetime import datetime,date
import csv
import time


from config import kafka_cluster, zookeeper_cluster
from kafka import KafkaProducer
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils
# from filter_messages import FilterMessage

class ReviewFiltering(object):

    def default(o):
        if isinstance(o, (date, datetime)):
            return o.isoformat()

    def convert_to_admin_format(row):

        message = {
            'client_id': row[0],
            'review_timestamp': row[1],
            'media_source': row[2],
            'media_entity_id': row[3],
            'review_id': row[4],
            'sentence_index': int(row[5]),
            'sentence_body': row[6],
            'rating': float(row[7]),
            'sentence_translated': row[8],
            'creator_id': row[9],
            'creator_name': row[10],
            'created_date': row[11],
            'url': row[12],
            'theme_sentiments': row[13]
        }
        ReviewFiltering.push(message)

    def open_file():

        with open('total.csv', 'rt')as f:
            data = csv.reader(f)
            i = 0
            for row in data:

                if (i > 0):
                    s = row[1]
                    row[1] = time.mktime(datetime.strptime(s, "%Y-%m-%d").timetuple())
                    p = row[13]
                    p = p.replace("\'", "\"")

                    p = json.loads(p)
                    row[13] = p

                    ReviewFiltering.convert_to_admin_format(row)
                i = i + 1


            #Add Filter data here
            # Optimize it


    def on_send_success(record_metadata):
        print("ReviewSitesConsumer:queue_filtered_data]", record_metadata.topic, record_metadata.partition, \
              record_metadata.offset)

    def on_send_error(excp):
        print(f'I am an errback with exception {excp}')

    def push(message):
        producer = KafkaProducer(
            value_serializer=lambda v: json.dumps(v, default=ReviewFiltering.default).encode('utf-8'),
            bootstrap_servers=kafka_cluster, retries=5)
        producer.send('mfi_annotation.sentence_with_theme',message).add_callback(ReviewFiltering.on_send_success).add_errback(ReviewFiltering.on_send_error)

        producer.flush()
        producer.close()


if __name__ == "__main__":
    sc = SparkContext(appName="ReviewFiltering")
    sc.setLogLevel("WARN")
    ssc = StreamingContext(sc, 5)

    ReviewFiltering.open_file()

    ssc.awaitTermination()
