import configparser
import os

config = configparser.ConfigParser()
config.read(os.path.expanduser('~/.setuserv/kafka.ini'))
config = config['Kafka']
_kafka_server = config['KAFKA_SERVER']
kafka_cluster = _kafka_server.split(',')
zookeeper_cluster = config['ZOOKEEPER_SERVER']
# REDIS_SERVER = config['REDIS_SERVER']
# REDIS_PORT = config['REDIS_PORT']
