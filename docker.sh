#!/usr/bin/env bash
git pull
eval $(aws ecr get-login --no-include-email)

docker build . -t data_test

current_epoch=$(date +%s)
docker tag data_test 002658610025.dkr.ecr.us-west-2.amazonaws.com/mfi/data_test:latest
docker tag data_test 002658610025.dkr.ecr.us-west-2.amazonaws.com/mfi/data_test:${current_epoch}

docker push 002658610025.dkr.ecr.us-west-2.amazonaws.com/mfi/data_test:latest
docker push 002658610025.dkr.ecr.us-west-2.amazonaws.com/mfi/data_test:${current_epoch}
